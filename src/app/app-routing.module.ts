import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LandingPageComponent} from "./landing-page/landing-page.component";
import {JobOverviewComponent} from "./job-overview/job-overview.component";
import {JobComponent} from "./job-details/job.component";

const routes: Routes = [
  {path: '', component: LandingPageComponent},
  {path: 'home', component: LandingPageComponent},
  {path: 'jobSearch', component: JobOverviewComponent},
  {path: 'jobDetails', children: [{
      path: ':jobId',
      component: JobComponent}
    ] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
