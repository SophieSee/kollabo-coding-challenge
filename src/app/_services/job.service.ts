import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';
import {Jobs} from "../job-details/types/jobs";
import {Job} from "../job-details/types/job";

/**
 * This Service makes the GET-Requests to the Job.ch API to get Information about the Jobs available.
 */
@Injectable({
  providedIn: 'root'
})
export class JobService {
  constructor(private http: HttpClient) {
  }

  /**
   * Retrieves the Job-Overview Information from the Search-API.
   */
  async getJobList() {
    // Not really required
    const headers = new HttpHeaders()
      .set('content-type', 'application/json');

    const categoryIds = '100';
    const rows = 20;
    const sort = 'date';
    const jobsPromise = this.http.get<Jobs>(
      `http://localhost:4200/api/v1/public/search?category-ids%5B%5D=${categoryIds}&rows=${rows}&sort=${sort}`,
      {headers}).toPromise();
    const jobs = await jobsPromise;
    return jobs?.documents;
  }

  /**
   * Retrieves the Job-Details for a particular Job, defined by the ID.
   * @param jobId ID of the Job to be requested.
   */
  async getJobDetails(jobId: number) {
    const jobDetailsPromise = this.http.get<Job>("http://localhost:4200/api/v1/public/search/job/" + jobId).toPromise();
    const jobDetails = await jobDetailsPromise;
    return jobDetails;
  }
}


