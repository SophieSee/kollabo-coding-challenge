/**
 * Defines a Job listed on the Job-Overview Page and on the Job-Details Page on Jobs.ch.
 * Only partially resembles the complete interface.
 */
export class Job {
  company_id: number;
  company_name: string;
  job_id: number;
  publication_date: Date;
  place: string;
  street: string;
  template_text: string;
  template_lead_text: string;
  preview?: string;
  title: string;
  images:[
    {
      context: string,
      url: string,
    }
  ]
}
