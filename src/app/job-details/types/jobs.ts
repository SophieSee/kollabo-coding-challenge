import {Job} from "./job";

/**
 * Defines a finite number of Jobs defined by the API from Jobs.ch
 */
export class Jobs {
  documents: Job[];
}
