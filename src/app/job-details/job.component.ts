import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {JobService} from "../_services/job.service";
import {Job} from "./types/job";

/**
 * This Component displays the Job-Details.
 */
@Component({
  selector: 'app-job',
  templateUrl: './job.component.html',
  styleUrls: ['./job.component.css']
})
export class JobComponent implements OnInit {
  jobId: number;
  job: Job | undefined;
  imageUrl: string | undefined;

  constructor(private jobService: JobService, private router: Router, private route: ActivatedRoute) {
    //get jobId from router to make api request
    const jobIdString = this.route.snapshot.paramMap.get('jobId');
    this.jobId = jobIdString as any as number;
  }

  ngOnInit(): void {
    this.initJobDetails(this.jobId);
  }

  /**
   * Initialises the Information needed to describe a particular Job.
   * @param jobId ID of the Job, that is being shown.
   */
  async initJobDetails(jobId: number) {
    const jobDetails = await this.jobService.getJobDetails(this.jobId);
    this.job = jobDetails;
    this.imageUrl = this.job?.images[0]?.url;
  }

  /**
   * Formats the Date to a user friendly format.
   * @param date Date when the job was posted.
   */
  getFormatedDate(date: Date | undefined) {
    if (date == null) {
      return "date undefined";
    }
    const dateType = new Date(date);
    const formatedDate = dateType.toLocaleDateString('de-DE');
    return formatedDate;
  }

}
