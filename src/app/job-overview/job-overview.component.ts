import { Component, OnInit } from '@angular/core';
import {JobService} from "../_services/job.service";
import {Job} from "../job-details/types/job";
import {ActivatedRoute, Router} from '@angular/router';

/**
 * This Component displays multiple Job-Previews.
 * TODO: Does not provide the user with the option to extend the search.
 */
@Component({
  selector: 'app-job-overview',
  templateUrl: './job-overview.component.html',
  styleUrls: ['./job-overview.component.css']
})
export class JobOverviewComponent implements OnInit {
  jobs: Job[] | undefined;

  constructor(private jobService: JobService) { }

  ngOnInit(): void {
    this.initJobs();
  }

  /**
   * Uses the Job-Service to get an Array of a certain number of Jobs from the API.
   */
  async initJobs(){
    const jobs = await this.jobService.getJobList();
    this.jobs = jobs;
  }

  /**
   * Formats the Date to a user friendly format.
   * @param date Date when the job was posted.
   */
  getFormatedDate(date: Date | undefined){
    if(date == null) {
      return "date undefined";
    }
    const dateType = new Date(date);
    const formatedDate = dateType.toLocaleDateString('de-DE');
    return formatedDate;
  }
}
