# KollaboInterview
Follow the instructions in this README to setup and run the application.

## Prerequisites
* Run `npm install -g @angular/cli` to install the Angular CLI.
* Run `npm install` to install the necessary packages.

## PWA

Run `npm run start-pwa` to build the PWA and start the webserver.  

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Caveats

* The PWA does not use the proxy that is used during development and CORS does therefore not work. In Production you could set up a proxy sever to mitigate this issue.
* Flex Layout (https://github.com/angular/flex-layout) is used in this Application, which is currently in BETA (I think ;)). Breaking changes could come up and require additional work.
* Application uses a dev proxy to get around the issues with CORS.
